﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtConsole.exemplo
{
    class Computador
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Memoria { get; set; }
        public int SSD { get; set; }
        public string Processador { get; set; } 
        public List<string> Softwares { get; set; }
        public Fornecedor Fornecedor { get; set; }
        public List<Lojas> Lojas { get; set; }
        
    }

    class Fornecedor
    {
        public string RSocial { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
    }

    class Lojas
    {
        public string Cidade { get; set; }
        public string Endereco { get; set; }
        public int Telefone { get; set; }
    }
}
