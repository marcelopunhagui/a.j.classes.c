﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace AtConsole.exemplo
{
    class Program
    {
        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/veiculo";

            // consumindo API
            Veiculo Veiculo = BuscarVeiculo(url);

            // exibindo dados retornados
            Console.WriteLine(
                string.Format(
                    "Marca: {0} - Modelo {1} - Ano {2}, Quilometragem {3}",
                    Veiculo.Marca,
                    Veiculo.Modelo,
                    Veiculo.Ano,
                    Veiculo.Quilometragem
                    )
                );

            // mantendo o console aberto
            Console.ReadLine();

        }
        // função destinada consumir a API que retornará os dados de um computador 
        public static Veiculo BuscarVeiculo(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Veiculo Veiculo = JsonConvert.DeserializeObject<Veiculo>(content);

            // retornando computador
            return Veiculo;
        }
    }
}
